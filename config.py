
# Configurations
# Maximum Buffer size to fetch at one go
PACKET_SIZE = 32767
'''
Virtual Host Settings:
VirtualHost = {
	'HostName': {
		'web': 'WebDirectory',
		'cache': 'CacheDirectory/',
	},
	...
}
'web' cotains the live web Directory
'cache' contains the cached data.
'''
VirtualHost = {
	'localhost': {
		'web': 'web/',
		'cache': 'cache/'
	},
}

'''
Port Number for Web Server
'''
PORT = 80

'''
Number of simultaneous clients
'''
MAX_CLIENTS = 5


'''
Should Imports be temporary or permanent?
Permanent would result in restart in web-server if file is changed
1 = Temporary
2 = Permanent
'''
IMPORT_TYPE = 1

'''
php_call_method:
1 = By Python(Limited)
2 = By PHP Server Communication(Full Fledged) (Need to type this from cmd: "php -S 127.0.0.1:8001")
'''
php_call_method = 1

'''
If APD Module is enabled, Add following instead of runkit_function_redefine:
override_function('header', '$a', 'header_overridden($a);');

Do not Edit below unless you know what you are doing...
'''
php_override_prefix = '''
/* Runkit */
$PYTHON_PHP_HEADERS = array();
$PYTHON_CURRENT_LOCATION = '';
runkit_function_redefine('header', '$a', 'header_overridden($a);');

function header_overridden($headers)
{
	global $PYTHON_PHP_HEADERS;
	if (!headers_sent($filename, $num)) {
		preg_match('/([^:]*): (.*)/', $headers, $matches);
		array_shift($matches);
		$prefixPython = '!!!!!PYTHON_START!!!!!'. $matches[0] .'!!!!!PYTHON_MID!!!!!'.$matches[1].'!!!!!PYTHON_END!!!!!';
		$PYTHON_PHP_HEADERS[] = $prefixPython;
	} else {
		print("Headers Already Sent at $filename:$num.");
	}
}

function add_data($json_requests)
{
	$_SERVER['REQUEST_METHOD'] = 'GET';
	// Json -> Array
	$json_array = json_decode($json_requests, TRUE);
	if (count($json_array["get"]) > 0) {
		add_get_data($json_array["get"]);
	}
	if (count($json_array["post"]) > 0) {
		add_post_data($json_array["post"]);
	}
	if (count($json_array["file"]) > 0) {
		add_file_data($json_array["file"]);
	}
}

function add_get_data($get_array)
{
	// Initialize Get Array
	$_GET = array();
	if (count($get_array) > 0) {
		foreach ($get_array as $key => $value) {
			if ($value == null) { $value = true; } else { $value = urldecode($value); }
			$_GET[urldecode($key)] = $value;
		}
		// Set Request Method to Get
		$_SERVER['REQUEST_METHOD'] = 'GET';
	}
}

function add_post_data($post_array)
{
	// Initialize POST Array
	$_POST = array();
	if (count($post_array) > 0) {
		foreach ($post_array as $key => $value) {
		if ($value == null) { $value = true; } else { $value = urldecode($value); }
			$_POST[urldecode($key)] = $value;
		}
		// Set Request Method to POST
		$_SERVER['REQUEST_METHOD'] = 'POST';
	}
}

function add_file_data($file_array)
{
	// Initialize FILES Array
	$_FILES = array();
	if (count($file_array) > 0) {
		foreach ($file_array as $key => $value) {
			$_FILES[urldecode($key)] = [
												"name" => urldecode($value["name"]),
												"data" => urldecode($value["data"]),
												"type" => urldecode($value["Content-Type"]),
												"error" => 0,
												"size" => urldecode($value["size"]),
												];
		}
		// Set Request Method to POST
		$_SERVER['REQUEST_METHOD'] = 'POST';
	}
}

function echo_headers() {
	global $PYTHON_PHP_HEADERS;
	print(implode("", $PYTHON_PHP_HEADERS));
}

function request_uri($r_uri) {
	$_SERVER['REQUEST_URI'] = $r_uri;
}
'''

'''
	Script to Execute Afer Completion of Original PHP Script
'''
php_override_postfix = '''
echo_headers();
'''

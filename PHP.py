import json
import re
import subprocess
from posixpath import basename, dirname
import requests
from config import *

class PHP:
	'''This class provides a stupid simple interface to PHP code.'''
	
	def __init__(self, uri, filename='404.html', requests = None, prefix = '', postfix = ''):
		'''
			filename = File to Execute
			prefix = Code to Execute before execution of file
			postfix = Code to execute afer execution of file
		'''
		self.requests = requests
		self.__file = filename
		self.filename = basename(filename)
		self.prefix = prefix
		self.postfix = postfix
		self.uri = uri
		self.directory = dirname(filename) +'/'
		self.__php = None

	def __submit(self):
		''' Opens the PHP Process and Gives the output '''
		'''
		(out, inp) = popen2.popen2('php')
		print >>inp, '<?php '
		'''
		p = subprocess.Popen(['php'], cwd=self.directory, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
		# Prefix
		if (self.prefix != ''):
			inp = '<?php '
			inp = inp + self.prefix
			inp = inp + 'add_data(\''
			inp = inp + json.dumps(self.requests)
			inp = inp + '\');'
			inp = inp + '$PYTHON_CURRENT_LOCATION = "'+ self.directory +'";'
			inp = inp + 'request_uri(\''+ self.uri +'\')'
			inp = inp + ' ?>'
		# Open The File
		fh = file(self.directory + self.filename, 'r')
		code = fh.read()
		fh.close()
		# Send Code
		inp = inp + code
		# Postfix
		if (self.postfix != ''):
			inp = inp + '<?php '
			inp = inp + self.postfix
			inp = inp + ' ?>'
		p.stdin.write(inp)
		g_stdout = p.communicate()[0]
		p.stdin.close()
		p.stdout.close()
		return g_stdout.decode()

	def get(self):
		if (php_call_method == 1):
			''' Run the PHP Script and get the Raw Output '''
			data = self.__submit()
			# Process PHP Header Data(TODO)
			header_list = re.findall('!!!!!PYTHON_START!!!!!([^!]*)!!!!!PYTHON_MID!!!!!([^!]*)!!!!!PYTHON_END!!!!!', data)
			data = re.sub('!!!!!PYTHON_START!!!!!([^!]*)!!!!!PYTHON_MID!!!!!([^!]*)!!!!!PYTHON_END!!!!!', '', data)
		elif (php_call_method == 2):
			#if (self.__php is None): # Run from Command Line
			#	self.php_test()
			(data, header_list) = self.php_test2()
		return (data, header_list)

	def get_json(self):
		''' Execute the PHP Script and interpret JSON as Python .'''
		out = self.__submit()
		return json.loads(out.read())

	def get_json_one(self):
		''' Execute the PHP Script that emits multiple json values (one per line), yield the next value. '''
		out = self.__submit()
		for line in out:
			line = line.strip()
			if line:
				yield json.loads(line)

	def php_test(self):
		self.__php = subprocess.Popen(['php', '-S', '127.0.0.1:8001'], cwd='web')

	def php_test2(self):
		file = 'http://127.0.0.1:8001/'+ self.__file[4:]
		r = requests.get(file)
		return (r.text, r.headers)

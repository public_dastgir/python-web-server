import RequestProcess
import PHP
import time
from config import *
import os
import sys

import Capturing
import traceback
import magic



class RequestParser:
	def __init__(self):
		'''
		Initialize the Variables
		'''
		# Set Some Variables
		self._response_headers = dict()
		self._WEB_DIR = self._CACHE_DIR = 'temp'
		#
		#request_method request_proto request_body __WEB_DIR __CACHE_DIR response_msg response_code


	def set_response(self, code):
		'''
			Sets the Response Code and Response Message for the request.
		'''
		self._response_code = code
		if (self._response_code == 200):
			self._response_msg = 'OK'
		elif (self._response_code == 302):
			self._response_msg = 'Found'
		elif (self._response_code == 400):
			self._response_msg = 'Bad Request'
		elif (self._response_code == 401):
			self._response_msg = 'Unauthorized'
		elif (self._response_code == 404):
			self._response_msg = 'Not Found'
		elif (self._response_code == 405):
			self._response_msg = 'Method not allowed'
		else:
			self._response_msg = 'Not Implemened'
		return

	'''
	Save the Data
	'''
	def setData(self, data):
		self._data = data
		return

	'''
		Splits Request Header and Body
		Headers are saved into list
	'''
	def parseHeaders(self):
		# There are 2 \n in the end.
		request_head, self._request_body = self._data.split('\n\n', 1)
		# First Line: Request, Others: Header
		request_head = request_head.splitlines()
		# GET URI PROTOCOL
		self._request_method, self._request_uri_complete, self._request_proto = request_head[0].split(' ', 3)
		
		# Save All Headers into Dictionary
		self._request_headers = dict(x.split(': ', 1) for x in request_head[1:])
		del request_head

	'''
		Checks if Request Method is valid or not.
		currently only GET/POST is valid.
	'''
	def checkValidRequestMethod(self):
		if (self._request_method != "GET" and self._request_method != "POST"):
			return False
		return True

	'''
		Parse the URI mentioned in the Request.
		URI are seperated to GET Requests too
	'''
	def parseURI(self):
		# Parse the URI (Seperate GET Requests)
		self._request_uris = self._request_uri_complete.split('?', 1)

		self._request_uri = self._request_uris[0]

		if (self._request_uri == '/'):
			self._request_uri = 'index.html'
		else:
			# Remove the First '/'
			self._request_uri = self._request_uri[1:]

	'''
		Parse and Process GET/Post Request
	'''
	def processRequest(self):
		'''
			Helper function to parse GET/POST Request
		'''
		requests = None
		rp = RequestProcess.RequestProcess(self._request_uris)
		(request_get_dict, status_code) = rp.parse_get_request()
		(request_post_dict, request_post_file_dict, status_code) = rp.parse_post_request(self._request_body, self._request_headers)
		if (status_code == 200):
			requests = {
						'get': request_get_dict,
						'post': request_post_dict,
						'file': request_post_file_dict
						}
		else:
			self.set_response(status_code)
		del self._request_uris
		del self._request_body
		return requests

	def __html_dir(self, headers):
		'''
			Checks the Directory of execution.
		'''
		directory = self._CACHE_DIR
		if ('Cache-Control' in headers):
			#request_headers = dict(x.split('', 1) for x in request_head[1:])	
			#Cache Control Remaining Headers Will not be implemented
			if (headers['Cache-Control'] == 'no-cache'):
				directory = self._WEB_DIR
		elif ('Pragma' in headers):
			if (headers['Pragma'] == 'no-cache'):
				directory = self._WEB_DIR
		return directory

	def get_file(self, headers):
		'''
			Checks for File Existence and returns the contents
			of file if exists, else returns 404 page.
		'''
		directory = self.__html_dir(headers)
		if (self._response_code != 200):
			self._request_uri = self._WEB_DIR + str(self._response_code) + '.html'
			if not os.path.isfile(self._request_uri):
				self._request_uri = 'error/' + str(self._response_code) + '.html'
		else:
			self.set_response(200)
		if not os.path.isfile(directory + self._request_uri):
			if (directory == self._CACHE_DIR):
				if not os.path.isfile(self._WEB_DIR + self._request_uri):
					(data, header_list, modified) = self.__execute_file('', 'error/404.html')
					self.set_response(404)
				else:
					(data, header_list, modified) = self.__execute_file(self._WEB_DIR, self._request_uri)
			else:
				(data, header_list, modified) = self.__execute_file('', 'error/404.html')
				self.set_response(404)
		else:
			(data, header_list, modified) = self.__execute_file(directory, self._request_uri)

		del self._requests
		del self._request_uri_complete
		del self._request_uri
		return (data, header_list, time.ctime(modified))

	def __execute_file(self, directory, filename):
		'''
			Executes the File(.py)/Gets the content of HTML and 
			sends the data and last modified information.
		'''
		global IMPORT_TYPE
		# Extra Header List
		header_list = []
		modified = os.path.getmtime(directory + filename)
		if (filename[len(filename)-3:] == ".py"):
			import_name = directory.replace("/", ".") + filename.replace("/", ".")[:len(filename)-3]
			# Execute Python main function (Remove .py from end.)
			ti = __import__(import_name)
			exec("temp_import = ti."+ filename.replace("/", ".")[:len(filename)-3])
			if "main" in dir(temp_import):
				with Capturing.Capturing() as data_list:
					try:
						temp_import.main(self._requests)
					except:
						print "Exception in user code:"
						print '-'*60
						traceback.print_exc(file=sys.stdout)
						print '-'*60
						pass
					#Capturing.Capturing.close()
				data = '<br/>'.join(data_list)
			else:
				(data, header_list, modified) = self.__execute_file('', 'error/404.html')
			if (IMPORT_TYPE == 1):
				del temp_import
				sys.modules.pop(import_name)
		elif (filename[len(filename)-4:] == ".php"):
			php = PHP.PHP(self._request_uri_complete, directory + filename, self._requests, php_override_prefix, php_override_postfix)
			(data, header_list) = php.get()
		else:
			mime = magic.Magic(mime=True)
			header_list.append(['Content-Type', mime.from_file(directory + filename)])
			fh = file(directory + filename, 'r')
			data = fh.read()
			fh.close()

		return (data, header_list, modified)

	def parseRequest(self):
		global VirtualHost
		# Parse The Request Headers and seperates the request_body
		self.parseHeaders()

		# Verify Host
		if ('Host' not in self._request_headers):
			self.set_response(400)
		else:
			if (':' in self._request_headers['Host']):
				host, port = self._request_headers['Host'].split(':', 1)
			else:
				host = self._request_headers['Host']
				port = 80
		
		# Restrict Request Method
		if (self.checkValidRequestMethod() == False):
			self.set_response(405)
		# Does this Host Exist?
		elif (host not in VirtualHost):
			self.set_response(401)
		# Everything is OK
		else:
			self.set_response(200)
			# Set Web and Cache DIR
			self._WEB_DIR = VirtualHost[host]['web']
			self._CACHE_DIR = VirtualHost[host]['cache']
		# ToDo Range Header	
		self.parseURI()
		# Process Get and Post Request
		self._requests = self.processRequest()

		# Get Data
		(data, header_list, last_modified) = self.get_file(self._request_headers)


		# Reponse Header
		self._response_headers['Date'] = time.strftime('%a, %d %b %Y %H:%M:%S GMT')
		self._response_headers['Server'] = 'Dastgir Web Server/1.1 (Python/Win32)'
		self._response_headers['Last-Modified'] = last_modified;
		self._response_headers['Content-Length'] = str(len(data))
		self._response_headers['Content-Type'] = 'text/html'
		if (self._request_headers['Connection'] == 'keep-alive'):
			self._response_headers['Keep-Alive'] = 'timeout=15, max=100'
			self._response_headers['Connection'] = 'keep-alive'
		else:
			self._response_headers['Connection'] = 'Closed'

		if (php_call_method == 1):
			for i in range(0, len(header_list)):
				self._response_headers[header_list[i][0]] = header_list[i][1]
		else:
			if type(header_list) == 'dict':
				for key, val in header_list.iteritems():
					self._response_headers[key] = val

		# Location: Set Temporary Redirect
		if 'Location' in self._response_headers:
			self.set_response(302)

		response =  'HTTP/1.1 ' + str(self._response_code) + ' '+ self._response_msg +'\n'
		# Generate Response
		for key, val in self._response_headers.iteritems():
			response = response + key +': '+ val +'\n'
		response = response + '\n' + data

		cinfo = self._request_headers['Connection']
		del self._request_headers

		# keep-alive = keep the connection alive (persistent connection.)
		if (cinfo == 'keep-alive'):
			return (response, True)
		else:
			return (response, False)

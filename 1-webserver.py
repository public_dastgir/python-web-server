'''
Can Do:
	Caching at execute_file
NOTE:
	It needs PHP-APD for Running PHP Files.
	It needs PHP-RUNKIT for Executing PHP Files (pear channel-discover zenovich.github.io/pear && pecl install zenovich/runkit)
	runkit.internal_override must be set to on

	Needs python-magic (pip)

'''
# python
import socket
import select
import time
from config import *

import RequestParser

def recv_all(sock):
	'''Receive everything from `sock`, until timeout occurs, meaning sender
	is exhausted, return result as string.'''

	prev_timeout = sock.gettimeout()
	try:
		# Set Timeout to very low in case sock.recv hangs(for not getting enough data)
		sock.settimeout(0.01)

		rdata = []
		i = 0
		while True:
			try:
				data = sock.recv(PACKET_SIZE)
				if (data == ''):
					i = i + 1
				else:
					i = 0
				if (i == 10):
					return('', 1)
				rdata.append(data)
			except socket.timeout:
				if len(rdata) == 0:
					sock.settimeout(prev_timeout)
					return ('', 1)
				else:
					sock.settimeout(prev_timeout)
					return (''.join(rdata), 0)

		# unreachable
	finally:
		# Set back to previous timeout
		sock.settimeout(prev_timeout)

# create an INET, STREAMing socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind to public host
sock.bind(('', PORT))
# Listen upto 5 connection request at a time.
sock.listen(MAX_CLIENTS)

SOCKET_LIST = list()
SOCKET_LIST.append(sock)

# Client List contains Key as IP:PORT
CLIENT_LIST = dict()

print("Running at "+ socket.gethostname())

while 1:
	# Get Read Sockets
	# 5 SEconds Timeout
	rsockets, wsockets, esockets = select.select(SOCKET_LIST, [], [], 15)
	for client in rsockets:
		# If Server Socket
		if client == sock:
			# Accept Connection
			(csockfd, address) = sock.accept()
			SOCKET_LIST.append(csockfd)
			print "Client("+ address[0] +") Connected at "+ time.strftime('%a, %d %b %Y %H:%M:%S') +"."

			# Set Timeout to 15 seconds
			csockfd.settimeout(15)
			CLIENT_LIST[str(address[0]) +":"+ str(address[1])] = {
																	't': time.time(),
																	'm': 100
			}
		else:
			req = RequestParser.RequestParser()
			(addr, port) = client.getpeername()
			addridx = str(addr) +":"+ str(port)
			# Get the Data (Returns 0 length if timeout)
			(data, error) = recv_all(client)
			# Some Error, Close the connection.
			if (error > 0 or len(data) == 0):
				client.close()
				SOCKET_LIST.remove(client)
				del CLIENT_LIST[addridx]
				continue
			# Substract 1 from max
			CLIENT_LIST[addridx]['m'] = CLIENT_LIST[addridx]['m'] - 1
			# Normalize the Data
			data = ''.join((line + '\n') for line in data.splitlines())

			# Feed the data to Request Parser
			req.setData(data)

			# Parse the Request
			(response, disconnect) = req.parseRequest()
			# Send the Data
			client.send(response)
			# Check if to disconnect the client or not
			if (disconnect == False or time.time() - 15 >= CLIENT_LIST[addridx]['t'] or CLIENT_LIST[addridx]['m'] <= 0):
				client.close()
				SOCKET_LIST.remove(client)
			else:
				client.settimeout(15)
			del req

sock.close()
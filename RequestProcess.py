import json
import re
import urllib
'''
Process GET/POST Request
'''
class RequestProcess:
	def __init__(self, request_uris):
		self._request_uris = request_uris

	def parse_get_request(self):
		''' 
			Parses GET Request /?a=b&c=d into request_get_dict
		'''
		# Get the ?PART
		if (len(self._request_uris) == 2):
			request_get = self._request_uris[1]
		else:
			return (None, 200)
		# Only Parse if valid request.
		if (request_get != ''):
			get_temp = request_get.split('&')
		else:
			return (None, 200)
		request_get_dict = dict()
		# Get all GET Requests
		if (len(get_temp) >= 1):
			for x in get_temp:
				get_temp2 = x.split('=', 1)
				if (len(get_temp2) == 2):
					request_get_dict[get_temp2[0]] = urllib.unquote(get_temp2[1]).decode('utf8')
				else:
					request_get_dict[get_temp2[0]] = True
		del request_get
		del get_temp
		del get_temp2
		return (request_get_dict, 200)

	def parse_post_request_form_encoded(self, request_body):
		'''
			Parses POST Request a=b&c=d into request_post_dict
		'''
		request_post_dict = dict()
		# Remove \n from end of body
		post_temp = request_body[:len(request_body)-1].split('&')
		for x in post_temp:
			post_temp2 = x.split('=', 1)
			if (len(post_temp2) == 2):
				request_post_dict[post_temp2[0]] = urllib.unquote(post_temp2[1]).decode('utf8')
			else:
				request_post_dict[post_temp2[0]] = True
		del post_temp
		del post_temp2
		return (request_post_dict, 200)

	def parse_post_request_json(self, request_body):
		'''
			Parses json Content-Type Request (POST)
		'''
		try:
			request_post_dict = json.loads(request_body)
		except:
			return (None, 400)
		return (request_post_dict, 200)

	def parse_post_request_multipart(self, request_headers, request_body):
		'''
			Parses MultiPart Data (Simple POST + Files)
		'''
		request_post_dict = dict()
		request_post_file_dict = dict()

		boundary_start = False
		search_type = 0
		skipLine = False
		# Check if boundary is defined
		if (request_headers['Content-Type'][21:21+8] == "boundary"):
			temp_data = ''
			boundary = request_headers['Content-Type'][21+10:]
			# Parse Line by Line
			for line in request_body.splitlines():
				# Boundary Occured?
				if boundary in line:
					# Save the Parsed Data
					if (search_type != 0):
						if (search_type == 1):	# Normal Post
							request_post_dict[name] = urllib.unquote(temp_data).decode('utf8')
						elif (search_type == 2): # File
							if (not (filename.strip() == '' or temp_data.strip() == '')):
								request_post_file_dict[name] = {
																'data': temp_data,
																'name': filename,
																'Content-Type': ctype,
																'size': len(temp_data)
															}
						del temp_data
						search_type = 0
					# Last Boundary
					if boundary+'--' in line:
						break
					boundary_start = True
					temp_data = ''
				elif boundary_start == True:
					# Check First Line: form-data (Contains name and filename)
					if ('Content-Disposition: form-data' in line):
						#Parse Name Data
						name = re.search('name="([^"]*)"', line)
						if not name:
							return (None, None, 400)
						name = name.group(0)
						name = name[6:len(name)-1]
						search_type = 1
						# Parse File Data
						filename = re.search('filename="([^"]*)"', line)
						if filename:
							filename = filename.group(0)
							filename = filename[10:len(filename)-1]
							filename = urllib.unquote(filename).decode('utf8')
							search_type = 2
						# Skip one Blank Line
						skipLine = True
					# Content-Type is defined for file only.
					elif ('Content-Type: ' in line):
						#Parse Name Data
						ctype = re.search('Content-Type: (.*)', line)
						ctype = ctype.group(0)
						ctype = ctype[14:len(ctype)]
					# Skip First Blank Line
					elif (skipLine == True):
						# Skip the Line
						skipLine = False
						continue
					# Save the Data
					elif (search_type == 1 or search_type == 2):	# Get Full Data
						if (temp_data == ''):
							temp_data = line
						else:
							temp_data = temp_data + "\n" + line
			
		else:
			return (None, None, 400)
		return (request_post_dict, request_post_file_dict, 200)

	def parse_post_request(self, request_body, request_headers):
		'''
			Detects the Type of Post Request and parses accordingly.
		'''
		request_post_dict = None
		request_post_file_dict = None
		status_code = 200
		# Get All Post Request
		if (request_body != '\n' and request_body != ''):
			# Parse Simple Post
			if (request_headers['Content-Type'] == 'application/x-www-form-urlencoded'):
				(request_post_dict, status_code) = self.parse_post_request_form_encoded()
			# Parse JSON
			elif (request_headers['Content-Type'] == 'application/json'):
				(request_post_dict, status_code) = self.parse_post_request_json(request_body)	
			# Parse Form Data
			elif (request_headers['Content-Type'][:19] == 'multipart/form-data'):
				(request_post_dict, request_post_file_dict, status_code) = self.parse_post_request_multipart(request_headers, request_body)
				# Check if Requested POST is parsed properly.
				if (status_code == 200):
					if (type(request_post_dict) == "dict" and len(request_post_dict) == 0):
						request_post_dict = None
					if (type(request_post_file_dict) == "dict" and len(request_post_file_dict) == 0):
						request_post_file_dict = None
			else:
				status_code = 400
		return (request_post_dict, request_post_file_dict, status_code)


	

# Python index


def main(requests):
	#print(requests['file']['fileUpload']['data'])
	if (requests['get'] is not None):
		print("Get Requests("+ str(len(requests['get'].items())) +"):")
		for key, value in requests['get'].iteritems():
			print(key +" = "+ str(value))
	else:
		print("No Get Requests")

	if (requests['post'] is not None):
		print("Post Requests("+ str(len(requests['post'].items())) +"):")
		for key, value in requests['post'].iteritems():
			print(key +" = "+ str(value))
	else:
		print("No POST Requests")

	if (requests['file'] is not None):
		print("Files Requests("+ str(len(requests['file'].items())) +"):")
		for key, value in requests['file'].iteritems():
			print(key +": Name="+ str(value['name']) +"; Size="+ str(value['size']) +"; Content-Type="+ str(value['Content-Type'])+". Data HIDDEN.")
	else:
		print("No Files Request")
	